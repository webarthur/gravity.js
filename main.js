var gcounter = 1
var Game = new Vue({

  el: '#stage',

  data: {
    stage: {
      w: 1200,
      h: 700
    },
    p1: {
      r: 100,
      d: .35,
      x: 100,
      y: 200,
      w: 50,
      h: 50,
      g: 1,
      gacc: .35,
      gcounter: 1,
      jump: 0,
      move: {
        x: 0,
        y: 0
      }
    },
    p2: {
      r: 100,
      d: .3,
      x: 200,
      y: 200,
      w: 50,
      h: 50,
      g: 1,
      gacc: .25,
      gcounter: 1,
      jump: 0,
      move: {
        x: 0,
        y: 0
      }
    },
    p3: {
      r: 100,
      d: .4,
      x: 300,
      y: 200,
      w: 50,
      h: 50,
      g: 1,
      gacc: .25,
      gcounter: 1,
      jump: 0,
      move: {
        x: 0,
        y: 0
      }
    },
    p4: {
      r: 100,
      d: .15,
      x: 400,
      y: 200,
      w: 50,
      h: 50,
      g: 10,
      gacc: .25,
      gcounter: 1,
      jump: 0,
      move: {
        x: 0,
        y: 0
      }
    }
  },

  methods: {

    move (p, x, y) {
      // check stage bottom
      if (p.y + p.h + y <= this.stage.h) {
        p.y += y
        p.g += p.gacc * p.gcounter
        p.gcounter += 1
      }
      else {
        p.g = p.g * p.d
        if (p.g < 1) {
          p.g = 0
        }
        p.g = -p.g
        p.y = this.stage.h - p.h
        p.gcounter = 1
      }
    },

    loop () {
      this.move(this.p1, this.p1.move.x, this.p1.move.y + this.p1.g)
      this.move(this.p2, this.p2.move.x, this.p2.move.y + this.p2.g)
      this.move(this.p3, this.p3.move.x, this.p3.move.y + this.p3.g)
      this.move(this.p4, this.p4.move.x, this.p4.move.y + this.p4.g)
      setTimeout(this.loop, 30)
    }

  }
})

Game.loop()
